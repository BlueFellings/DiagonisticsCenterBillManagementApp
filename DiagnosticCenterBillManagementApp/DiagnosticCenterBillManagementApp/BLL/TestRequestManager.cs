﻿using DiagnosticCenterBillManagementApp.DAL;
using DiagnosticCenterBillManagementApp.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DiagnosticCenterBillManagementApp.BLL
{
    public class TestRequestManager
    {
        TestRequestGateway testRequestGateway = new TestRequestGateway();
        
        public TestRequest GetTestFee(string testName)
        {
            TestRequest tr = testRequestGateway.GetTestFee(testName);
            return tr;
        }

        public List<TestRequest> GetUnpaidTestRequestList(string patientName)
        {
            List<TestRequest> testRequestList = testRequestGateway.GetTestRequestList(patientName);
            return testRequestList;
        }

        public int Insert(TestRequest testRequest)
        {
            return testRequestGateway.Insert(testRequest);
        }

        public int InsertAll(List<TestRequest> testRequestList)
        {
            return testRequestGateway.InsertAll(testRequestList);
        }

        public int DeletePatient()
        {
            return testRequestGateway.DeletePatient();
        }

    }
}