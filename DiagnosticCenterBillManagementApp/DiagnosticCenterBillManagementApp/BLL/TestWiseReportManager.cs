﻿using DiagnosticCenterBillManagementApp.DAL;
using DiagnosticCenterBillManagementApp.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DiagnosticCenterBillManagementApp.BLL
{
    public class TestWiseReportManager
    {
        TestWiseReportGateway testWiseReportGateway = new TestWiseReportGateway();

        public List<TestWiseReportDao> GetTestWiseList(DateTime fromDate, DateTime toDate)
        {
            List<TestWiseReportDao> testWiseReportList = testWiseReportGateway.GetTestWiseList(fromDate, toDate);
            return testWiseReportList;
        }
        public decimal GetTotalAmount()
        {
            return testWiseReportGateway.GetTotalAmount();
        }
    }
}