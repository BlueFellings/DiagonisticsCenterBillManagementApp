﻿using DiagnosticCenterBillManagementApp.DAL;
using DiagnosticCenterBillManagementApp.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DiagnosticCenterBillManagementApp.BLL
{
    public class TypeWiseReportManager
    {
        TypeWiseReportGateway typeWiseReportGateway = new TypeWiseReportGateway();
        public List<TypeWiseReportDao> GetTypeWiseList(DateTime fromDate, DateTime toDate)
        {
            List<TypeWiseReportDao> typeWiseReportList = typeWiseReportGateway.GetTypeWiseList(fromDate, toDate);
            return typeWiseReportList;
        }
        public decimal GetTotalAmount()
        {
            return typeWiseReportGateway.GetTotalAmount();
        }
    }
}