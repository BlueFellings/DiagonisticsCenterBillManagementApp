﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace DiagnosticCenterBillManagementApp.BLL
{
    public class TestGateway
    {
        public static string connectionString = ConfigurationManager.ConnectionStrings["DiagnosticCeneterDB"].ConnectionString;
        private SqlConnection connection;

        public TestGateway()
        {
            connection = new SqlConnection(connectionString);
        }
        public int Insert(Test test)
        {
            string query = "INSERT INTO TestInfoSetup(TestName,Fee,TestType) Values('" + test.TestName + "' , '" + test.Fee + "' , '" + test.Type + "')";
            SqlCommand command = new SqlCommand(query, connection);

            connection.Open();
            int rowAffected = -1;
            rowAffected = command.ExecuteNonQuery();
            connection.Close();

            return rowAffected;
        }

        public List<Test> GetTestList()
        {
            string query = ("SELECT * FROM TestInfoSetup");
            SqlCommand command = new SqlCommand(query, connection);
            connection.Open();
            SqlDataReader reader = command.ExecuteReader();
            List<Test> testList = new List<Test>();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    int sl = Convert.ToInt32(reader["Id"]);
                    string name = reader["TestName"].ToString();
                    decimal fee = Convert.ToDecimal(reader["Fee"]);
                    string type = reader["TestType"].ToString();

                    Test test = new Test(sl, name, fee, type);
                    testList.Add(test);
                }
            }
            connection.Close();
            return testList;
        }

        public bool UniqueTestCheck(string name)
        {
            string query = ("SELECT * FROM TestInfoSetup WHERE TestName='" + name + "'");
            SqlCommand command = new SqlCommand(query, connection);
            connection.Open();
            SqlDataReader reader = command.ExecuteReader();
            bool isTypeExist = false;
            if (reader.HasRows)
            {
                isTypeExist = true;
            }
            connection.Close();
            return isTypeExist;
        }

    }
}