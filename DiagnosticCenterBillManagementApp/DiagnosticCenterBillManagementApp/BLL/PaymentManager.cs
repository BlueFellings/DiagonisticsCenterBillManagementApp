﻿using DiagnosticCenterBillManagementApp.DAL;
using DiagnosticCenterBillManagementApp.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DiagnosticCenterBillManagementApp.BLL
{
    public class PaymentManager
    {
        PaymentGateway paymentGateway = new PaymentGateway();
        public TestRequest GetPayingAmount(string mobile, int billNo)
        {
            TestRequest testRequest = paymentGateway.GetPayingAmount(mobile, billNo);
            return testRequest;
        }

        public int UpdateRequestEntry(string mobile, int billNo)
        {
            return paymentGateway.UpdateRequestEntry(mobile,billNo);
        }

        public List<UnpaidBillProperty> GetUnpaidList(DateTime fromDate, DateTime toDate)
        {
            List<UnpaidBillProperty> testRequestList = paymentGateway.GetUnpaidList(fromDate, toDate);
            return testRequestList;
        }
        public decimal GetTotalAmount()
        {
            return paymentGateway.GetTotalAmount();
        }

    }
}