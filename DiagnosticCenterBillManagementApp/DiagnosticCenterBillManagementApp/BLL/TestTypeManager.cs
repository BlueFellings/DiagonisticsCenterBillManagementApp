﻿using DiagnosticCenterBillManagementApp.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DiagnosticCenterBillManagementApp.DAL;

namespace DiagnosticCenterBillManagementApp.BLL
{
    public class TestTypeManager
    {
        TestTypeGateway testTypeGateway = new TestTypeGateway();

        public int Insert(TestType testType)
        {
            if (UniqueTestTypeCheck(testType.Type) == true)
                return testTypeGateway.Insert(testType);
            else
                return 0;
        }

        public List<TestType> GetTestList()
        {
            List<TestType> testTypeList = testTypeGateway.GetTestList();
            return testTypeList;
        }

        public bool UniqueTestTypeCheck(string name)
        {
            if (testTypeGateway.UniqueTestTypeCheck(name) == true)
            {
                throw new Exception("This Test Type already exists !!");
            }
            return true;
        }

    }
}