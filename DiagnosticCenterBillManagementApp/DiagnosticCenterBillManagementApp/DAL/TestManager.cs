﻿using DiagnosticCenterBillManagementApp.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DiagnosticCenterBillManagementApp.DAL
{
    public class TestManager
    {
        TestGateway testGateway = new TestGateway();

        public int Insert(Test test)
        {
            if (UniqueTestCheck(test.TestName) == true)
                return testGateway.Insert(test);
            return 0;
        }

        public List<Test> GetTestList()
        {
            List<Test> testList = testGateway.GetTestList();
            return testList;
        }

        public bool UniqueTestCheck(string name)
        {
            if (testGateway.UniqueTestCheck(name) == true)
            {
                throw new Exception("This Test Name Already Exists !!");
            }
            return true;
        }
    }
}