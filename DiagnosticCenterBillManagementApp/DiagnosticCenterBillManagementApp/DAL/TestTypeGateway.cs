﻿using DiagnosticCenterBillManagementApp.DAO;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace DiagnosticCenterBillManagementApp.DAL
{
    public class TestTypeGateway
    {
        public static string connectionString = ConfigurationManager.ConnectionStrings["DiagnosticCeneterDB"].ConnectionString;
        private SqlConnection connection;

        public TestTypeGateway()
        {
            connection = new SqlConnection(connectionString);
        }

        public int Insert(TestType testType)
        {
            string query = "INSERT INTO TestType(Name) Values('" + testType.Type + "')";
            SqlCommand command = new SqlCommand(query, connection);

            connection.Open();
            int rowAffected = -1;
            rowAffected = command.ExecuteNonQuery();
            connection.Close();

            return rowAffected;
        }

        public bool UniqueTestTypeCheck(string name)
        {
            string query = ("SELECT * FROM TestType WHERE Name='" + name + "'");
            SqlCommand command = new SqlCommand(query, connection);
            connection.Open();
            SqlDataReader reader = command.ExecuteReader();
            bool isTypeExist = false;
            if (reader.HasRows)
            {
                isTypeExist = true;
            }
            connection.Close();
            return isTypeExist;
        }

        public List<TestType> GetTestList()
        {
            string query = ("SELECT * FROM TestType");
            SqlCommand command = new SqlCommand(query, connection);
            connection.Open();
            SqlDataReader reader = command.ExecuteReader();
            List<TestType> testTypeList = new List<TestType>();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    int sl = Convert.ToInt32(reader["Id"]);
                    string name = reader["Name"].ToString();

                    TestType testType = new TestType(sl, name);
                    testTypeList.Add(testType);
                }
            }
            connection.Close();
            return testTypeList;
        }
    }
}