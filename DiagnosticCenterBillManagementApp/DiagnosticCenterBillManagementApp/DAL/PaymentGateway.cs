﻿using DiagnosticCenterBillManagementApp.DAO;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace DiagnosticCenterBillManagementApp.DAL
{
    public class PaymentGateway
    {
        public static string connectionString = ConfigurationManager.ConnectionStrings["DiagnosticCeneterDB"].ConnectionString;
        private SqlConnection connection;

        public decimal TotalAmount { get; set; }

        public PaymentGateway()
        {
            connection = new SqlConnection(connectionString);            

        }

        public TestRequest GetPayingAmount(string mobile, int billNo)
        {
            string query;
            query = (@"SELECT EntryDate, BillNo, (SELECT SUM(Fee) FROM PatientTestRequest WHERE " +
                     "Mobile= '" + mobile + "' OR BillNo= '" + billNo + "' AND IsPaid= '" + 0 + "') " +
                     "AS Total FROM PatientTestRequest WHERE Mobile= '" + mobile + "' OR " +
                     "BillNo= '" + billNo + "' AND IsPaid= '" + 0 + "' GROUP BY BillNo, EntryDate");

            SqlCommand command = new SqlCommand(query, connection);
            connection.Open();
            SqlDataReader reader = command.ExecuteReader();
            decimal fee = 0;
            DateTime dueDate = System.DateTime.Now;
            if (reader.Read())
            {
                fee = Convert.ToDecimal(reader["Total"]);
                dueDate = Convert.ToDateTime(reader["EntryDate"]);
            }
            TestRequest testRequest = new TestRequest(fee, dueDate);
            connection.Close();
            return testRequest;
        }

        public List<UnpaidBillProperty> GetUnpaidList(DateTime fromDate, DateTime toDate)
        {
            decimal totalFee = 0;
            string from = fromDate.ToString("yyyy-MM-dd");
            string to = toDate.ToString("yyyy-MM-dd");
            string query = ("SELECT BillNo as BillNumber, Mobile, PatientName, SUM(Fee) AS BillAmount FROM PatientTestRequest WHERE EntryDate BETWEEN '" + from + "' AND '" + to + "' AND IsPaid= '" + 0 + "' GROUP BY PatientName, BillNo, Mobile");


            SqlCommand command = new SqlCommand(query, connection);
            connection.Open();
            SqlDataReader reader = command.ExecuteReader();
            List<UnpaidBillProperty> testRequestList = new List<UnpaidBillProperty>();

            if (reader.HasRows)
            {
                int sl = 0;
                while (reader.Read())
                {
                    sl++;
                    int billNumber = Convert.ToInt32(reader["BillNumber"]);
                    string contactNo =  reader["Mobile"].ToString();
                    string patientName = reader["PatientName"].ToString(); ;
                    decimal billAmount = Convert.ToDecimal(reader["BillAmount"]);
                    totalFee += billAmount;
                    UnpaidBillProperty unpaidRequest = new UnpaidBillProperty(sl, billNumber, contactNo, patientName, billAmount);
                    testRequestList.Add(unpaidRequest);
                }
                this.TotalAmount = totalFee;
                GetTotalAmount();
            }
            connection.Close();
            return testRequestList;
        }

        public decimal GetTotalAmount()
        {
            return this.TotalAmount;
        }

        public int UpdateRequestEntry(string mobile, int billNo)
        {
            string query = ("UPDATE PatientTestRequest SET IsPaid= '" + 1 + "' WHERE Mobile= '" + mobile + "' OR BillNo= '" + billNo + "' AND IsPaid= '" + 0 + "'");

            SqlCommand command = new SqlCommand(query, connection);
            connection.Open();
            int rowAffected = -1;
            rowAffected = command.ExecuteNonQuery();
            
            connection.Close();
            return rowAffected;
        }
    }
}