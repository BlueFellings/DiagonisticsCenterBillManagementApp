﻿using DiagnosticCenterBillManagementApp.DAO;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace DiagnosticCenterBillManagementApp.DAL
{
    public class TypeWiseReportGateway
    {
        public decimal TotalAmount { get; set; }

        public static string connectionString = ConfigurationManager.ConnectionStrings["DiagnosticCeneterDB"].ConnectionString;
        private SqlConnection connection;

        public TypeWiseReportGateway()
        {
            connection = new SqlConnection(connectionString);
        }

        public List<TypeWiseReportDao> GetTypeWiseList(DateTime fromDate, DateTime toDate)
        {
            decimal totalFee = 0;
            string from = fromDate.ToString("yyyy-MM-dd");
            string to = toDate.ToString("yyyy-MM-dd");
            string query = ("SELECT TestType as TestTypeName, SUM(Fee) AS TotalAmount, COUNT(TestType) AS TotalTest FROM PatientTestRequest WHERE EntryDate BETWEEN '" + from + "' AND '" + to + "' GROUP BY TestType, Fee");

            SqlCommand command = new SqlCommand(query, connection);
            connection.Open();
            SqlDataReader reader = command.ExecuteReader();
            List<TypeWiseReportDao> typeWiseReportList = new List<TypeWiseReportDao>();
            if (reader.HasRows)
            {
                int sl = 0;
                while (reader.Read())
                {
                    sl++;
                    string testType = reader["TestTypeName"].ToString();
                    decimal totalAmount = Convert.ToDecimal(reader["TotalAmount"]);
                    int totalTest = Convert.ToInt32(reader["TotalTest"]);
                    totalFee += totalAmount;
                    TypeWiseReportDao typeWiseReportDao = new TypeWiseReportDao(sl, testType, totalTest, totalAmount);
                    typeWiseReportList.Add(typeWiseReportDao);
                }
                this.TotalAmount = totalFee;
                GetTotalAmount();
            }
            connection.Close();
            return typeWiseReportList;
        }

        public decimal GetTotalAmount()
        {
            return this.TotalAmount;
        }
    }
}