﻿using DiagnosticCenterBillManagementApp.DAO;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace DiagnosticCenterBillManagementApp.DAL
{
    public class TestWiseReportGateway
    {
        public decimal TotalAmount { get; set; }

        public static string connectionString = ConfigurationManager.ConnectionStrings["DiagnosticCeneterDB"].ConnectionString;
        private SqlConnection connection;

        public TestWiseReportGateway()
        {
            connection = new SqlConnection(connectionString);
        }

        public List<TestWiseReportDao> GetTestWiseList(DateTime fromDate, DateTime toDate)
        {
            decimal totalFee = 0;
            string from = fromDate.ToString("yyyy-MM-dd");
            string to = toDate.ToString("yyyy-MM-dd");
            string query = ("SELECT Test, SUM(Fee) AS TotalAmount, COUNT(Test) AS TotalTest FROM PatientTestRequest WHERE EntryDate BETWEEN '" + from + "' AND '" + to + "' GROUP BY Test, Fee");

            SqlCommand command = new SqlCommand(query, connection);
            connection.Open();
            SqlDataReader reader = command.ExecuteReader();
            List<TestWiseReportDao> testWiseReportList = new List<TestWiseReportDao>();
            if (reader.HasRows)
            {
                int sl = 0;
                while (reader.Read())
                {
                    sl++;
                    string testName = reader["Test"].ToString();
                    decimal totalAmount = Convert.ToDecimal(reader["TotalAmount"]);
                    int totalTest = Convert.ToInt32(reader["TotalTest"]);
                    totalFee += totalAmount;
                    TestWiseReportDao testWiseReportDao = new TestWiseReportDao(sl, testName, totalTest, totalAmount);
                    testWiseReportList.Add(testWiseReportDao);
                }
                this.TotalAmount = totalFee;
                GetTotalAmount();
            }
            connection.Close();
            return testWiseReportList;
        }

        public decimal GetTotalAmount()
        {
            return this.TotalAmount;
        }
    }
}