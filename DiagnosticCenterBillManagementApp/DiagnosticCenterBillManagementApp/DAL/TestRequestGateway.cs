﻿using DiagnosticCenterBillManagementApp.DAO;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace DiagnosticCenterBillManagementApp.DAL
{
    public class TestRequestGateway
    {
        public static string connectionString = ConfigurationManager.ConnectionStrings["DiagnosticCeneterDB"].ConnectionString;
        private SqlConnection connection;

        public int sl;

        public TestRequestGateway()
        {
            connection = new SqlConnection(connectionString);
        }

        public int Insert(TestRequest request)
        {
            string query = "INSERT INTO Patient(PatientName,Mobile,BirthDate,Test,EntryDate, Fee, TestType) Values('" + request.PatintName + "' , '" + request.Mobile + "' , '" + request.BirthDate + "' , '" + request.Test + "', '" + request.EntryDate + "' , '" + request.Fee + "' , '" + request.TestType + "')";
            SqlCommand command = new SqlCommand(query, connection);

            connection.Open();
            int rowAffected = -1;
            rowAffected = command.ExecuteNonQuery();
            connection.Close();

            return rowAffected;
        }

        public List<TestRequest> GetTestRequestList(string PatientName)
        {

            decimal totalFee = 0;
            string query = ("SELECT * FROM Patient WHERE PatientName = '" + PatientName + "' ");
            SqlCommand command = new SqlCommand(query, connection);
            connection.Open();
            SqlDataReader reader = command.ExecuteReader();
            List<TestRequest> testRequestList = new List<TestRequest>();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    int sl = Convert.ToInt32(reader["Id"]);
                    string patientName = reader["PatientName"].ToString();
                    string mobile = reader["Mobile"].ToString();
                    DateTime birth = Convert.ToDateTime(reader["BirthDate"]);
                    decimal fee = Convert.ToDecimal(reader["Fee"]);
                    string test = reader["Test"].ToString();
                    DateTime entry = Convert.ToDateTime(reader["EntryDate"]);
                    string testType = reader["TestType"].ToString();
                    totalFee += fee;

                    TestRequest testRequest = new TestRequest(patientName, birth, mobile, test, fee, sl, entry, totalFee, testType);
                    testRequestList.Add(testRequest);
                }
            }
            connection.Close();
            return testRequestList;
        }

        public void GenerateBillNo(string patientName)
        {
            //Generate BilNo
            string query = "INSERT INTO BillNoGenarate(Name) Values('" + patientName + "')";
            SqlCommand command = new SqlCommand(query, connection);
            connection.Open();
            command.ExecuteNonQuery();
            connection.Close();
        }

        public int GetBillNo(string patientName)
        {
            //Fetch Bill No
            string query = "Select BillNo From BillNoGenarate WHERE Name = '" + patientName + "'";
            SqlCommand command = new SqlCommand(query, connection);
            connection.Open();
            SqlDataReader reader = command.ExecuteReader();
            int billNo = 0;

            if (reader.Read())
            {
                billNo = Convert.ToInt32(reader["BillNo"]);
            }
            connection.Close();
            return billNo;
        }

        public int InsertAll(List<TestRequest> testRequestList)
        {
            string patientName = testRequestList[0].PatintName;
            GenerateBillNo(patientName);
            int billNo = GetBillNo(patientName);
            int rowAffected = -1;
            for (int i = 0; i < testRequestList.Count; i++)
            {
                string query = "INSERT INTO PatientTestRequest(PatientName,Mobile,BirthDate,Test,EntryDate,Fee,IsPaid,TestType,BillNo) Values('" + testRequestList[i].PatintName + "' , '" + testRequestList[i].Mobile + "' , '" + testRequestList[i].BirthDate + "' , '" + testRequestList[i].Test + "', '" + testRequestList[i].EntryDate + "' , '" + testRequestList[i].Fee + "', '" + 0 + "', '" + testRequestList[i].TestType + "', '" + billNo + "')";
                SqlCommand command = new SqlCommand(query, connection);

                connection.Open();
                rowAffected = command.ExecuteNonQuery();
                if(rowAffected!=1)
                {
                    return rowAffected;
                }
                connection.Close();
            }        
            return rowAffected;
        }

        public int DeletePatient()
        {
            string query = "TRUNCATE TABLE Patient";
            SqlCommand command = new SqlCommand(query, connection);

            connection.Open();
            int rowAffected = -1;
            rowAffected = command.ExecuteNonQuery();
            connection.Close();

            return rowAffected;
        }

        public TestRequest GetTestFee(string testName)
        {
            string query = ("SELECT Fee, TestType FROM TestInfoSetup WHERE TestName= '" + testName + "' ");
            SqlCommand command = new SqlCommand(query, connection);
            connection.Open();
            SqlDataReader reader = command.ExecuteReader();
            decimal fee = 0;
            string testType = null;
            if (reader.Read())
            {
                fee = Convert.ToDecimal(reader["Fee"]);
                testType = reader["TestType"].ToString();
            }
            TestRequest tr = new TestRequest(fee,testType);
            
            connection.Close();
            return tr;
        }
    }
}