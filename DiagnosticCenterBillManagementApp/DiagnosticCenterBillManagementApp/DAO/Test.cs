﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DiagnosticCenterBillManagementApp
{
    public class Test
    {
        public string TestName { get; set; }
        public string Type { get; set; }
        public decimal Fee { get; set; }
        public int TestSl { get; set; }

        public Test(string name, decimal fee, string type) 
        {
            this.TestName = name;
            this.Fee = fee;
            this.Type = type;
        }

        public Test(int sl, string name, decimal fee, string type)
            : this(name, fee, type)
        {
            this.TestSl = sl;
        }
    }
}