﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DiagnosticCenterBillManagementApp.DAO
{
    public class TestType
    {
        public string Type { get; set; }
        public int TestSl { get; set; }

        public TestType(string type) 
        {
            this.Type = type;
        }

        public TestType(int sl, string type)
            : this(type)
        {
            this.TestSl = sl;
        }
    }
}