﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DiagnosticCenterBillManagementApp.DAO
{
    public class TypeWiseReportDao
    {
        public int Sl { get; set; }
        public string Test_Type_Name { get; set; }
        public int Total_No_of_Test { get; set; }
        public decimal Total_Amount { get; set; }

        public TypeWiseReportDao(int sl, string typeName, int totalTest, decimal totalAmt) 
        {
            this.Sl = sl;
            this.Test_Type_Name = typeName;
            this.Total_No_of_Test = totalTest;
            this.Total_Amount = totalAmt;
        }
    }
}