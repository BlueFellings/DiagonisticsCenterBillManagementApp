﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DiagnosticCenterBillManagementApp.DAO
{
    public class TestRequest
    {
        public int Sl { get; set; }
        public int BillNo { get; set; }
        public string PatintName { get; set; }
        public DateTime BirthDate { get; set; }
        public string Mobile { get; set; }
        public string Test { get; set; }
        public decimal Fee { get; set; }
        public decimal TotalFee { get; set; }
        public DateTime EntryDate { get; set; }
        public int TotalTest { get; set; }
        public string TestType { get; set; }

        public TestRequest(decimal fee, string testType)
        {
            this.Fee = fee;
            this.TestType = testType;
        }

        public TestRequest(decimal fee, DateTime entry)
        {
            this.Fee = fee;
            this.EntryDate = entry;
        }

        public TestRequest(int sl, string testName, decimal TotalAmount, int totalTest)
        {
            this.Sl = sl;
            this.Test = testName;
            this.TotalFee = TotalAmount;
            this.TotalTest = totalTest;
        }

        public TestRequest(int sl, int billNumber, string contactNo, string patientName, decimal billAmount)
        {
            this.Sl = sl;
            this.BillNo = billNumber;
            this.Mobile = contactNo;
            this.PatintName = patientName;
            this.Fee = billAmount;
        }

        public TestRequest(string name, DateTime birth, string mobile, string test, decimal fee, string testType) 
        {
            this.PatintName = name;
            this.BirthDate = birth;
            this.Mobile = mobile ;
            this.Test = test;
            this.Fee = fee;
            this.EntryDate = System.DateTime.Now;
            this.TestType = testType;
        }

        public TestRequest(string name, DateTime birth, string mobile, string test, decimal fee, int sl, DateTime entry, decimal totalFee, string testType)
            :this(name,birth,mobile,test,fee,testType)
        {
            this.Sl = sl;
            this.EntryDate = entry;
            this.TotalFee = totalFee;
        }
    }
}