﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DiagnosticCenterBillManagementApp.DAO
{
    public class TestWiseReportDao
    {
        public int Sl { get; set; }
        public string TestName { get; set; }
        public int TotalTest { get; set; }
        public decimal TotalAmount { get; set; }

        public TestWiseReportDao(int sl, string name, int totalTest, decimal totalAmt) 
        {
            this.Sl = sl;
            this.TestName = name;
            this.TotalTest = totalTest;
            this.TotalAmount = totalAmt;
        }
    }
}