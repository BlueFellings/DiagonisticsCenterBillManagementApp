﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DiagnosticCenterBillManagementApp.DAO
{
    public class UnpaidBillProperty
    {
        public int Sl { get; set; }
        public int Bill_No { get; set; }
        public string Contact_no { get; set; }
        public string Patint_Name { get; set; }
        public decimal Bill_Amount { get; set; }
        
        public UnpaidBillProperty(int sl, int billNumber, string contactNo, string patientName, decimal billAmount)
        {
            this.Sl = sl;
            this.Bill_No = billNumber;
            this.Contact_no = contactNo;
            this.Patint_Name = patientName;
            this.Bill_Amount = billAmount;
        }

    }
}