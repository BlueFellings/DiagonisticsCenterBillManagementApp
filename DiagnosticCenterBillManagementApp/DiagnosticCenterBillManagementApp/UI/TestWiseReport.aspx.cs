﻿using CSVLib;
using DiagnosticCenterBillManagementApp.BLL;
using DiagnosticCenterBillManagementApp.DAO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DiagnosticCenterBillManagementApp.UI
{
    public partial class TestWiseReport : System.Web.UI.Page
    {
        TestWiseReportManager testWiseReportManager = new TestWiseReportManager();
        protected void Page_Load(object sender, EventArgs e)
        {
            gridDiv.Visible = false;
        }

        protected void showButton_Click(object sender, EventArgs e)
        {
            message.Text = "";
            if (fromDateTxt.Text != "" && toDateTxt.Text != "")
            {
                testWiseListGridView.DataSource = testWiseReportManager.GetTestWiseList(Convert.ToDateTime(fromDateTxt.Text), Convert.ToDateTime(toDateTxt.Text));
                testWiseListGridView.DataBind();

                if (testWiseListGridView.Rows.Count == 0)
                {
                    emptyMessage.Text = "No Record Found!!";
                    pdfButton.Visible = false;
                    totalFeeLabel.Visible = false;
                }
                else
                {
                    gridDiv.Visible = true;
                    totalFeeLabel.Text = "Total : " + testWiseReportManager.GetTotalAmount().ToString();
                    pdfButton.Visible = true;
                    totalFeeLabel.Visible = true;
                    emptyMessage.Text = "";
                }
            }
            else
            {
                message.Text = "Please Choose From date And To date First !!";
            }
        }

        protected void pdfButton_Click(object sender, EventArgs e)
        {
            int rowNumber = testWiseListGridView.Rows.Count;
            if (testWiseListGridView.Rows.Count > 0)
            {
                PdfPTable pdfPTable = new PdfPTable(testWiseListGridView.HeaderRow.Cells.Count);
                foreach (TableCell headerCell in testWiseListGridView.HeaderRow.Cells)
                {
                    Font font = new Font();
                    font.Color = new BaseColor(testWiseListGridView.HeaderStyle.ForeColor);

                    PdfPCell pdfCell = new PdfPCell(new Phrase(headerCell.Text));
                    pdfCell.BackgroundColor = new BaseColor(testWiseListGridView.HeaderStyle.BackColor);
                    pdfPTable.AddCell(pdfCell);
                }
                foreach (GridViewRow gridViewRow in testWiseListGridView.Rows)
                {
                    foreach (TableCell tableCell in gridViewRow.Cells)
                    {
                        Font font = new Font();
                        font.Color = new BaseColor(testWiseListGridView.HeaderStyle.ForeColor);

                        PdfPCell pdfCell = new PdfPCell(new Phrase(tableCell.Text));
                        pdfCell.BackgroundColor = new BaseColor(testWiseListGridView.RowStyle.BackColor);
                        pdfPTable.AddCell(pdfCell);
                    }
                }

                Document pdfDocument = new Document(PageSize.A4, 20f, 20f, 90f, 20f);
                PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDocument, Response.OutputStream);

                pdfDocument.Open();

                PdfContentByte cb = pdfWriter.DirectContent;
                ColumnText pageNameCt = new ColumnText(cb);
                Phrase pageNameText = new Phrase("Test Wise Report");

                pageNameCt.SetSimpleColumn(pageNameText, 200f, 790f, 75f, 80f, 20f, Element.ALIGN_LEFT);
                pageNameCt.Go();

                pdfDocument.Add(pdfPTable);

                ColumnText totalCt = new ColumnText(cb);
                Phrase totalText = new Phrase(totalFeeLabel.Text);
                totalCt.SetSimpleColumn(totalText, 500f, 100f, 380f, 860f - (rowNumber * 20), 150f, Element.ALIGN_RIGHT);
                totalCt.Go();

                pdfDocument.Close();

                Response.ContentType = "application/pdf";
                Response.AppendHeader("content-disposition", "attacment;filename=TestWiseReport.pdf");
                Response.Write(pdfDocument);
                Response.Flush();
                Response.End();

                totalFeeLabel.Visible = true;
            }
        }
    }
}