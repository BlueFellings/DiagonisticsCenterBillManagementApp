﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TestInfoForm.aspx.cs" Inherits="DiagnosticCenterBillManagementApp.TestInfoForm" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head runat="server">
    <title>Test Setup</title>
    <link href="../Style/CommonStyle.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">

        <div id="navigation">
	    <a href ="SetTestTypeForm.aspx" runat="server" class="LinkButton"><asp:Button class="navButton" ID="testName" runat="server" Text="Test Type Setup" PostBackUrl="SetTestTypeForm.aspx"></asp:Button></a>|
	    <a href ="TestInfoForm.aspx" runat="server" class="LinkButton"><asp:Button class="navButton" ID="testInfo" runat="server" Text="Test Setup" PostBackUrl="TestInfoForm.aspx"></asp:Button></a>|
	    <a href ="PatientsTestEntryForm.aspx" runat="server" class="LinkButton"><asp:Button class="navButton" ID="patientTestEntry" Text="Test Request Entry" runat="server" PostBackUrl="PatientsTestEntryForm.aspx" ></asp:Button></a>|
        <a href ="Payment.aspx" runat="server" class="LinkButton"><asp:Button class="navButton" ID="Button1" Text="Payment" runat="server" PostBackUrl="Payment.aspx" ></asp:Button></a>|
        <a href ="UnpaidBill.aspx" runat="server" class="LinkButton"><asp:Button class="navButton" ID="Button4" Text="Unpaid Bill" runat="server" PostBackUrl="UnpaidBill.aspx" ></asp:Button></a>|
        <a href ="TestWiseReport.aspx" runat="server" class="LinkButton"><asp:Button class="navButton" ID="Button2" Text="Test Wise Report" runat="server" PostBackUrl="TestWiseReport.aspx" ></asp:Button></a>|
        <a href="TypeWiseReport.aspx" runat="server" class="LinkButton"><asp:Button class="navButton" ID="Button3" Text="Type Wise Report" runat="server" PostBackUrl="TypeWiseReport.aspx" ></asp:Button></a>
        </div>

        <div style="height: 80px;" >
        </div>

    <div class="CenterDivStyle">
        <fieldset class="FiedSetStyle">
            <legend class="LegendStyle">Test Setup</legend>
            <table class="TableStyle">
                <tr>
                    <td>
                        <asp:label ID="name" runat="server" CssClass="TdLabelStyle">Test Name</asp:label>
                        <asp:textbox id="testNameTxt" runat="server" class="TdTextBoxStyle"></asp:textbox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:label ID="fee" runat="server" CssClass="TdLabelStyle">Fee</asp:label>
                        <asp:textbox ID="feeTxt" runat="server" class="TdTextBoxStyle"></asp:textbox>
                        <asp:label ID="bdt" runat="server" class="TextStyle">BDT</asp:label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:label ID="type" runat="server" CssClass="TdLabelStyle">Test Type</asp:label>
                        <asp:DropDownList ID="testTypeDDL" runat="server" class="DDLStyle" AutoPostBack="true" AppendDataBoundItems="true"></asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="save" runat="server" Text="Save" CssClass="TableButtonStyle" OnClick="save_Click"></asp:Button>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:label ID="message" runat="server" CssClass="OperationMessage"></asp:label>
                    </td>
                </tr>
            </table>
            <div id="gridDiv" runat="server" class="TableStyle">
                    <asp:GridView ID="testInfoGridView" runat="server" AutoGenerateColumns="False" BackColor="White" BorderColor="#999999" 
                    BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Vertical" Width="100%">
                    <AlternatingRowStyle BackColor="#a7c6a5" />
                        <Columns>
                            <asp:TemplateField HeaderText="SL" HeaderStyle-Width="10%" ItemStyle-Width="10%" >
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("TestSl") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Test Name" HeaderStyle-Width="30%" ItemStyle-Width="30%">
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("TestName") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Fee" HeaderStyle-Width="30%" ItemStyle-Width="30%">
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("Fee") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Type" HeaderStyle-Width="30%" ItemStyle-Width="30%">
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("Type") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />  
                    <HeaderStyle BackColor="#186d04" Font-Bold="True" ForeColor="White" />  
                    <PagerStyle BackColor="#808080" ForeColor="Black" HorizontalAlign="Center" />  
                    <RowStyle BackColor="#98dc93" ForeColor="Black" />  
                    <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="Black" />  
                    <SortedAscendingCellStyle BackColor="#F1F1F1" />  
                    <SortedAscendingHeaderStyle BackColor="#0000A9" />  
                    <SortedDescendingCellStyle BackColor="#CAC9C9" />  
                    <SortedDescendingHeaderStyle BackColor="#000065" />
                    </asp:GridView>
                <asp:Label ID="emptyMessage" runat="server" ></asp:Label>
            </div>
            <div style="height: 100px;" >
            </div>
        </fieldset>
    </div>
    </form>
</body>
</html>
