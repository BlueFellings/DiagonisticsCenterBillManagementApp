﻿using DiagnosticCenterBillManagementApp.BLL;
using DiagnosticCenterBillManagementApp.DAL;
using DiagnosticCenterBillManagementApp.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DiagnosticCenterBillManagementApp
{
    public partial class PatientsTestEntryForm : System.Web.UI.Page
    {
        TestRequestManager testRequestManager = new TestRequestManager();
        TestManager testManager = new TestManager();
        TestRequest request;

        protected void Page_Load(object sender, EventArgs e)
        {
            feeRow.Visible = false;

            if (!IsPostBack)
            {
                testTypeDDL.Items.Clear();
                LoadTestTypeInDDL();
                testRequestManager.DeletePatient();
            }

            if(priviewGrid.Rows.Count > 0)
            {
                gridDiv.Visible = true;
            }
            else
                gridDiv.Visible = false;

        }

        public void LoadTestTypeInDDL()
        {
            testTypeDDL.DataSource = testManager.GetTestList();
            testTypeDDL.DataValueField = "TestName";
            testTypeDDL.DataTextField = "TestName";
            testTypeDDL.Items.Insert(0, new ListItem("--- Please Select a Test ---", "0"));
            testTypeDDL.DataBind();
        }

        protected void add_Click(object sender, EventArgs e)
        {
            if (patientNameTxt.Text != "" && dobTxt.Text != "" && mobileTxt.Text != "")
            {
                if (testTypeDDL.SelectedItem.Value != "0")
                {
                    TestRequest tft = testRequestManager.GetTestFee(testTypeDDL.SelectedItem.Value);
                    feeTxt.Text = tft.Fee.ToString();
                    decimal fee = Convert.ToDecimal(tft.Fee);
                    string testType = tft.TestType.ToString();
                    request = new TestRequest(patientNameTxt.Text, Convert.ToDateTime(dobTxt.Text), mobileTxt.Text, testTypeDDL.SelectedItem.Value, fee, testType);

                    try
                    {
                        int inserted = (testRequestManager.Insert(request));
                        if (inserted == 1)
                        {
                            message.Text = "Inserted Succesfully!!";
                        }
                        else
                            message.Text = "Insertion Failed!!";
                    }
                    catch(Exception ex)
                    {
                        message.Text = ex.Message;
                    }

                    feeRow.Visible = true;
                    LoadTestInPriviewGrid(patientNameTxt.Text);
                    gridDiv.Visible = true;
                    saveButton.Visible = true;
                }
            }
            else
            {
                message.Text = "Please Fill all the info!!";
            }
        }

        private void ClearTextBoxs()
        {
            testRequestManager.DeletePatient();
            patientNameTxt.Text = null;
            dobTxt.Text = null;
            mobileTxt.Text = null;
            //feeRow.Visible = false;
            testTypeDDL.SelectedIndex = 0;
            totalFeeLabel.Text = null;
            message.Text = null;
        }

        protected void LoadTestInPriviewGrid(string patientName)
        {
            List<TestRequest> tr = new List<TestRequest>();
            tr = testRequestManager.GetUnpaidTestRequestList(patientName);
            decimal totalFee =0;

            try
            {
                priviewGrid.DataSource = tr;
                priviewGrid.DataBind();
                priviewGrid.Visible = true;

                int totalCount = tr.Count;
                totalFeeLabel.Text = "Total " + tr[totalCount - 1].TotalFee.ToString();
                emptyMessage.Text = "";
            }
            catch (Exception ex)
            {
                emptyMessage.Text = ex.Message;
            }
            
        }

        protected void testTypeDDL_SelectedIndexChanged(object sender, EventArgs e)
        {
            TestRequest tft = testRequestManager.GetTestFee(testTypeDDL.SelectedItem.Value);
            feeTxt.Text = tft.Fee.ToString();
            feeRow.Visible = true;
            message.Text = "";
            emptyMessage.Text = "";
            if (priviewGrid.Rows.Count > 0)
                gridDiv.Visible = true;
            else
                gridDiv.Visible = false;
        }

        protected void saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                List<TestRequest> unpaidTestRequestList = new List<TestRequest>();
                unpaidTestRequestList = testRequestManager.GetUnpaidTestRequestList(patientNameTxt.Text);

                int inserted = testRequestManager.InsertAll(unpaidTestRequestList);
                if (inserted == 1)
                {
                    emptyMessage.Text = "Inserted Succesfully!!";
                    testRequestManager.DeletePatient();
                    priviewGrid.Visible = false;
                    saveButton.Visible = false;
                    ClearTextBoxs();
                }
                else
                    message.Text = "Insertion Failed!!";
                priviewGrid.DataSource = null;
                priviewGrid.DataBind();
                
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
        }
    }
}