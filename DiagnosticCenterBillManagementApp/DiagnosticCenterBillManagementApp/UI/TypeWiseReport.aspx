﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TypeWiseReport.aspx.cs" Inherits="DiagnosticCenterBillManagementApp.UI.TypeWiseReport" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head runat="server">
    <title>Type Wise Report</title>
    <link href="../Style/CommonStyle.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">

        <div id="navigation">
	    <a href ="SetTestTypeForm.aspx" runat="server" class="LinkButton"><asp:Button class="navButton" ID="testName" runat="server" Text="Test Type Setup" PostBackUrl="SetTestTypeForm.aspx"></asp:Button></a>|
	    <a href ="TestInfoForm.aspx" runat="server" class="LinkButton"><asp:Button class="navButton" ID="testInfo" runat="server" Text="Test Setup" PostBackUrl="TestInfoForm.aspx"></asp:Button></a>|
	    <a href ="PatientsTestEntryForm.aspx" runat="server" class="LinkButton"><asp:Button class="navButton" ID="patientTestEntry" Text="Test Request Entry" runat="server" PostBackUrl="PatientsTestEntryForm.aspx" ></asp:Button></a>|
        <a href ="Payment.aspx" runat="server" class="LinkButton"><asp:Button class="navButton" ID="Button1" Text="Payment" runat="server" PostBackUrl="Payment.aspx" ></asp:Button></a>|
        <a href ="UnpaidBill.aspx" runat="server" class="LinkButton"><asp:Button class="navButton" ID="Button4" Text="Unpaid Bill" runat="server" PostBackUrl="UnpaidBill.aspx" ></asp:Button></a>|
        <a href ="TestWiseReport.aspx" runat="server" class="LinkButton"><asp:Button class="navButton" ID="Button2" Text="Test Wise Report" runat="server" PostBackUrl="TestWiseReport.aspx" ></asp:Button></a>|
        <a href="TypeWiseReport.aspx" runat="server" class="LinkButton"><asp:Button class="navButton" ID="Button3" Text="Type Wise Report" runat="server" PostBackUrl="TypeWiseReport.aspx" ></asp:Button></a>
        </div>

        <div style="height: 80px;" >
        </div>

    <div class="CenterDivStyle">
        <fieldset class="FiedSetStyle">
            <legend class="LegendStyle">Type Wise Report</legend>
            <table class="TableStyle">
                <tr>
                    <td>
                        <asp:label ID="fromDate" runat="server" class="TdLabelStyle">From Date</asp:label>
                        <asp:textbox id="fromDateTxt" runat="server" class="TdTextBoxStyle" TextMode="Date" ></asp:textbox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:label ID="toDate" runat="server" class="TdLabelStyle">To Date</asp:label>
                        <asp:textbox id="toDateTxt" runat="server" class="TdTextBoxStyle" TextMode="Date" ></asp:textbox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="showButton" runat="server" Text="Show" class="TableButtonStyle" OnClick="showButton_Click" ></asp:Button>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:label ID="message" runat="server" class="OperationMessage"></asp:label>
                    </td>
                </tr>
            </table>
            <div id="gridDiv" runat="server" class="TableStyle">
                <asp:GridView ID="TypeWiseListGridView" runat="server" BackColor="White" BorderColor="#999999" 
                    BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Vertical" Width="100%">
                    <AlternatingRowStyle BackColor="#a7c6a5" />
                     <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />  
                    <HeaderStyle BackColor="#186d04" Font-Bold="True" ForeColor="White" />  
                    <PagerStyle BackColor="#808080" ForeColor="Black" HorizontalAlign="Center" />  
                    <RowStyle BackColor="#98dc93" ForeColor="Black" />  
                    <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="Black" />  
                    <SortedAscendingCellStyle BackColor="#F1F1F1" />  
                    <SortedAscendingHeaderStyle BackColor="#0000A9" />  
                    <SortedDescendingCellStyle BackColor="#CAC9C9" />  
                    <SortedDescendingHeaderStyle BackColor="#000065" />
                </asp:GridView>
                <div style="height: 10px;" >
                </div>
                <asp:label ID="totalFeeLabel" runat="server" style="margin-left:575px; font: bold 18px arial; font-family:Century;"></asp:label>
                <br /><br />
                <asp:Button ID="pdfButton" runat="server" Text="PDF" class="TableButtonStyle" OnClick="pdfButton_Click" ></asp:Button>
                <br/>
                <asp:Label ID="emptyMessage" runat="server" Text="" class="OperationMessage"></asp:Label>
            </div>
            <div style="height: 100px;" >
            </div>
        </fieldset>
    </div>
    </form>
</body>
</html>