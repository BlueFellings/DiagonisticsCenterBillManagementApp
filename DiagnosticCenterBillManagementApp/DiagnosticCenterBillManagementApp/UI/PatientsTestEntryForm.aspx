﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PatientsTestEntryForm.aspx.cs" Inherits="DiagnosticCenterBillManagementApp.PatientsTestEntryForm" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Test Request Entry</title>
    <link href="../Style/CommonStyle.css" rel="stylesheet" />
</head>
<body>   
    <form id="form1" runat="server">
        <div id="navigation">
	    <a href ="SetTestTypeForm.aspx" runat="server" class="LinkButton"><asp:Button class="navButton" ID="testName" runat="server" Text="Test Type Setup" PostBackUrl="SetTestTypeForm.aspx"></asp:Button></a>|
	    <a href ="TestInfoForm.aspx" runat="server" class="LinkButton"><asp:Button class="navButton" ID="testInfo" runat="server" Text="Test Setup" PostBackUrl="TestInfoForm.aspx"></asp:Button></a>|
	    <a href ="PatientsTestEntryForm.aspx" runat="server" class="LinkButton"><asp:Button class="navButton" ID="patientTestEntry" Text="Test Request Entry" runat="server" PostBackUrl="PatientsTestEntryForm.aspx" ></asp:Button></a>|
        <a href ="Payment.aspx" runat="server" class="LinkButton"><asp:Button class="navButton" ID="Button1" Text="Payment" runat="server" PostBackUrl="Payment.aspx" ></asp:Button></a>|
        <a href ="UnpaidBill.aspx" runat="server" class="LinkButton"><asp:Button class="navButton" ID="Button4" Text="Unpaid Bill" runat="server" PostBackUrl="UnpaidBill.aspx" ></asp:Button></a>|
        <a href ="TestWiseReport.aspx" runat="server" class="LinkButton"><asp:Button class="navButton" ID="Button2" Text="Test Wise Report" runat="server" PostBackUrl="TestWiseReport.aspx" ></asp:Button></a>|
        <a href="TypeWiseReport.aspx" runat="server" class="LinkButton"><asp:Button class="navButton" ID="Button3" Text="Type Wise Report" runat="server" PostBackUrl="TypeWiseReport.aspx" ></asp:Button></a>
        </div>

        <div style="height: 80px;" >
        </div>

    <div class="CenterDivStyle">
        <fieldset class="FiedSetStyle">
            <legend class="LegendStyle">Test Request Entry</legend>
            <table class="TableStyle">
                <tr>
                    <td>
                        <asp:label ID="name" runat="server" class="TdLabelStyle">Patient's Name</asp:label>
                        <asp:textbox id="patientNameTxt" runat="server" class="TdTextBoxStyle"></asp:textbox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:label ID="Label2" runat="server" class="TdLabelStyle">Date od Birth</asp:label>
                        <asp:textbox id="dobTxt" runat="server" class="TdTextBoxStyle" TextMode="Date"></asp:textbox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:label ID="Label3" runat="server" class="TdLabelStyle">Mobile No</asp:label>
                        <asp:textbox id="mobileTxt" runat="server" class="TdTextBoxStyle"></asp:textbox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:label ID="type" runat="server" class="TdLabelStyle">Select Test</asp:label>
                        <asp:DropDownList ID="testTypeDDL" runat="server" class="DDLStyle" AutoPostBack="true" AppendDataBoundItems="true" OnSelectedIndexChanged="testTypeDDL_SelectedIndexChanged"></asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td id="feeRow" runat="server">
                        <asp:label ID="feeLabel" runat="server" class="TdLabelStyle">Fee</asp:label>
                        <asp:textbox id="feeTxt" runat="server" ReadOnly="true" class="TdTextBoxStyle"></asp:textbox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="add" runat="server" Text="Add" class="TableButtonStyle" OnClick="add_Click"></asp:Button>
                    </td>
                </tr>
                
                <tr>
                    <td>
                        <asp:label ID="message" runat="server" class="OperationMessage"></asp:label>
                    </td>
                </tr>
            </table>
            <div id="gridDiv" runat="server" class="TableStyle">
                <asp:GridView ID="priviewGrid" runat="server" AutoGenerateColumns="False" BackColor="White" BorderColor="#999999" 
                    BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Vertical" Width="100%">
                    <AlternatingRowStyle BackColor="#a7c6a5" />
                    <Columns>
                        <asp:TemplateField HeaderText="SL" HeaderStyle-Width="20%" ItemStyle-Width="20%">
                            <ItemTemplate>
                                <asp:Label ID="slItem" runat="server" Text='<%# Bind("Sl") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Test" HeaderStyle-Width="40%" ItemStyle-Width="40%">
                            <ItemTemplate>
                                <asp:Label ID="nameItem" runat="server" Text='<%# Bind("Test") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Fee" HeaderStyle-Width="40%" ItemStyle-Width="40%">
                            <ItemTemplate>
                                <asp:Label ID="feeItem" runat="server" Text='<%# Bind("Fee") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />  
                    <HeaderStyle BackColor="#186d04" Font-Bold="True" ForeColor="White" />  
                    <PagerStyle BackColor="#808080" ForeColor="Black" HorizontalAlign="Center" />  
                    <RowStyle BackColor="#98dc93" ForeColor="Black" />  
                    <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="Black" />  
                    <SortedAscendingCellStyle BackColor="#F1F1F1" />  
                    <SortedAscendingHeaderStyle BackColor="#0000A9" />  
                    <SortedDescendingCellStyle BackColor="#CAC9C9" />  
                    <SortedDescendingHeaderStyle BackColor="#000065" />
                </asp:GridView>
                <asp:label ID="totalFeeLabel" runat="server" class="TableButtonStyle"></asp:label>
                <asp:Button ID="saveButton" runat="server" Text="Save" class="TableButtonStyle" OnClick="saveButton_Click"></asp:Button>
                <br/>
                <asp:Label ID="emptyMessage" runat="server" Text="" class="OperationMessage"></asp:Label>
                <div style="height: 25px;" >
            </div>
            </div>
            <div style="height: 100px;" >
            </div>
        </fieldset>
    </div>
    </form>
</body>
</html>

