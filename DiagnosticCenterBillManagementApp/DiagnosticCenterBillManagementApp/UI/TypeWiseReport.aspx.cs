﻿using CSVLib;
using DiagnosticCenterBillManagementApp.BLL;
using DiagnosticCenterBillManagementApp.DAO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DiagnosticCenterBillManagementApp.UI
{
    public partial class TypeWiseReport : System.Web.UI.Page
    {
        TypeWiseReportManager typeWiseReportManager = new TypeWiseReportManager();
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void showButton_Click(object sender, EventArgs e)
        {
            if (fromDateTxt.Text != null && toDateTxt.Text != null)
            {
                TypeWiseListGridView.DataSource = typeWiseReportManager.GetTypeWiseList(Convert.ToDateTime(fromDateTxt.Text), Convert.ToDateTime(toDateTxt.Text));
                TypeWiseListGridView.DataBind();

            }
            if (fromDateTxt.Text != "" && toDateTxt.Text != "")
            {
                message.Text = "";
                TypeWiseListGridView.DataSource = typeWiseReportManager.GetTypeWiseList(Convert.ToDateTime(fromDateTxt.Text), Convert.ToDateTime(toDateTxt.Text));
                TypeWiseListGridView.DataBind();

                if (TypeWiseListGridView.Rows.Count == 0)
                {
                    emptyMessage.Text = "No Record Found!!";
                    pdfButton.Visible = false;
                    totalFeeLabel.Visible = false;
                }
                else
                {
                    gridDiv.Visible = true;
                    totalFeeLabel.Text = "Total : " + typeWiseReportManager.GetTotalAmount().ToString();
                    pdfButton.Visible = true;
                    totalFeeLabel.Visible = true;
                    emptyMessage.Text = "";
                }
            }
            else
            {
                message.Text = "Please Choose From date And To date First !!";
            }
        }

        protected void pdfButton_Click(object sender, EventArgs e)
        {
            int rowNumber = TypeWiseListGridView.Rows.Count;
            if (TypeWiseListGridView.Rows.Count > 0)
            {
                PdfPTable pdfPTable = new PdfPTable(TypeWiseListGridView.HeaderRow.Cells.Count);
                foreach (TableCell headerCell in TypeWiseListGridView.HeaderRow.Cells)
                {
                    Font font = new Font();
                    font.Color = new BaseColor(TypeWiseListGridView.HeaderStyle.ForeColor);

                    PdfPCell pdfCell = new PdfPCell(new Phrase(headerCell.Text));
                    pdfCell.BackgroundColor = new BaseColor(TypeWiseListGridView.HeaderStyle.BackColor);
                    pdfPTable.AddCell(pdfCell);
                }
                foreach (GridViewRow gridViewRow in TypeWiseListGridView.Rows)
                {
                    foreach (TableCell tableCell in gridViewRow.Cells)
                    {
                        Font font = new Font();
                        font.Color = new BaseColor(TypeWiseListGridView.HeaderStyle.ForeColor);

                        PdfPCell pdfCell = new PdfPCell(new Phrase(tableCell.Text));
                        pdfCell.BackgroundColor = new BaseColor(TypeWiseListGridView.RowStyle.BackColor);
                        pdfPTable.AddCell(pdfCell);
                    }
                }

                Document pdfDocument = new Document(PageSize.A4, 20f, 20f, 90f, 20f);
                PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDocument, Response.OutputStream);

                pdfDocument.Open();

                PdfContentByte cb = pdfWriter.DirectContent;
                ColumnText pageNameCt = new ColumnText(cb);
                Phrase pageNameText = new Phrase("Type Wise Report");

                pageNameCt.SetSimpleColumn(pageNameText, 200f, 790f, 75f, 80f, 20f, Element.ALIGN_LEFT);
                pageNameCt.Go();

                pdfDocument.Add(pdfPTable);

                ColumnText totalCt = new ColumnText(cb);
                Phrase totalText = new Phrase(totalFeeLabel.Text);
                totalCt.SetSimpleColumn(totalText, 500f, 100f, 380f, 860f - (rowNumber * 20), 150f, Element.ALIGN_RIGHT);
                totalCt.Go();

                pdfDocument.Close();

                Response.ContentType = "application/pdf";
                Response.AppendHeader("content-disposition", "attacment;filename=TypeWiseReport.pdf");
                Response.Write(pdfDocument);
                Response.Flush();
                Response.End();

                totalFeeLabel.Visible = true;
            }
        }
    }
}