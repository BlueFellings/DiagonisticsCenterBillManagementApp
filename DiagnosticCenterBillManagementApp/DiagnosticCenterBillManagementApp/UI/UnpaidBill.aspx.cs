﻿using CSVLib;
using DiagnosticCenterBillManagementApp.BLL;
using DiagnosticCenterBillManagementApp.DAO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;

namespace DiagnosticCenterBillManagementApp.UI
{
    public partial class UnpaidBill : System.Web.UI.Page
    {
        PaymentManager paymentManager = new PaymentManager();
        protected void Page_Load(object sender, EventArgs e)
        {
            gridDiv.Visible = false;
        }

        protected void showButton_Click(object sender, EventArgs e)
        {
            message.Text = "";
            if (fromDateTxt.Text != "" && toDateTxt.Text != "") 
            {
                unpaidListGridView.DataSource = paymentManager.GetUnpaidList(Convert.ToDateTime(fromDateTxt.Text), Convert.ToDateTime(toDateTxt.Text));
                unpaidListGridView.DataBind();
                
                if (unpaidListGridView.Rows.Count == 0)
                {
                    emptyMessage.Text = "No Record Found!!";
                    gridDiv.Visible = true;
                    pdfButton.Visible = false;
                    totalFeeLabel.Visible = false;
                }
                else
                {
                    gridDiv.Visible = true;
                    totalFeeLabel.Text = "Total : " + paymentManager.GetTotalAmount().ToString();
                    pdfButton.Visible = true;
                    totalFeeLabel.Visible = true;
                    emptyMessage.Text = "";
                }
            }
            else
            {
                message.Text = "Please Choose From date And To date First !!";
            }
        }

        protected void pdfButton_Click(object sender, EventArgs e)
        {
            int rowNumber = unpaidListGridView.Rows.Count;
            if (unpaidListGridView.Rows.Count > 0)
            { 
                PdfPTable pdfPTable = new PdfPTable(unpaidListGridView.HeaderRow.Cells.Count);
                foreach (TableCell headerCell in unpaidListGridView.HeaderRow.Cells)
                {
                    Font font = new Font();
                    font.Color = new BaseColor(unpaidListGridView.HeaderStyle.ForeColor);

                    PdfPCell pdfCell = new PdfPCell(new Phrase(headerCell.Text));
                    pdfCell.BackgroundColor = new BaseColor(unpaidListGridView.HeaderStyle.BackColor);
                    pdfPTable.AddCell(pdfCell);
                }
                foreach (GridViewRow gridViewRow in unpaidListGridView.Rows)
                {
                    foreach (TableCell tableCell in gridViewRow.Cells)
                    {
                        Font font = new Font();
                        font.Color = new BaseColor(unpaidListGridView.HeaderStyle.ForeColor);

                        PdfPCell pdfCell = new PdfPCell(new Phrase(tableCell.Text));
                        pdfCell.BackgroundColor = new BaseColor(unpaidListGridView.RowStyle.BackColor);
                        pdfPTable.AddCell(pdfCell);
                    }
                }

                Document pdfDocument = new Document(PageSize.A4, 20f, 20f, 90f, 20f);
                PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDocument, Response.OutputStream);

                pdfDocument.Open();

                PdfContentByte cb = pdfWriter.DirectContent;
                ColumnText pageNameCt = new ColumnText(cb);
                Phrase pageNameText = new Phrase("Unpaid Bill Report");
                //width, bottom, left,
                pageNameCt.SetSimpleColumn(pageNameText, 200f, 790f, 75f, 80f, 20f, Element.ALIGN_LEFT);
                pageNameCt.Go();

                pdfDocument.Add(pdfPTable);

                ColumnText totalCt = new ColumnText(cb);
                Phrase totalText = new Phrase(totalFeeLabel.Text);
                totalCt.SetSimpleColumn(totalText, 500f, 100f, 380f, 860f-(rowNumber*20), 150f, Element.ALIGN_RIGHT);
                totalCt.Go();

                pdfDocument.Close();

                Response.ContentType = "application/pdf";
                Response.AppendHeader("content-disposition", "attacment;filename=UnpaidBill.pdf");
                Response.Write(pdfDocument);
                Response.Flush();
                Response.End();

                totalFeeLabel.Visible = true;
            }

        }
    }
}