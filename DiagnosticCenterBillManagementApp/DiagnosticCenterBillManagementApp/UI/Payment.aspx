﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Payment.aspx.cs" Inherits="DiagnosticCenterBillManagementApp.UI.Payment" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head runat="server">
    <title>Payment</title>
    <link href="../Style/CommonStyle.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">

        <div id="navigation">
	    <a href ="SetTestTypeForm.aspx" runat="server" class="LinkButton"><asp:Button class="navButton" ID="testName" runat="server" Text="Test Type Setup" PostBackUrl="SetTestTypeForm.aspx"></asp:Button></a>|
	    <a href ="TestInfoForm.aspx" runat="server" class="LinkButton"><asp:Button class="navButton" ID="testInfo" runat="server" Text="Test Setup" PostBackUrl="TestInfoForm.aspx"></asp:Button></a>|
	    <a href ="PatientsTestEntryForm.aspx" runat="server" class="LinkButton"><asp:Button class="navButton" ID="patientTestEntry" Text="Test Request Entry" runat="server" PostBackUrl="PatientsTestEntryForm.aspx" ></asp:Button></a>|
        <a href ="Payment.aspx" runat="server" class="LinkButton"><asp:Button class="navButton" ID="Button1" Text="Payment" runat="server" PostBackUrl="Payment.aspx" ></asp:Button></a>|
        <a href ="UnpaidBill.aspx" runat="server" class="LinkButton"><asp:Button class="navButton" ID="Button4" Text="Unpaid Bill" runat="server" PostBackUrl="UnpaidBill.aspx" ></asp:Button></a>|
        <a href ="TestWiseReport.aspx" runat="server" class="LinkButton"><asp:Button class="navButton" ID="Button2" Text="Test Wise Report" runat="server" PostBackUrl="TestWiseReport.aspx" ></asp:Button></a>|
        <a href="TypeWiseReport.aspx" runat="server" class="LinkButton"><asp:Button class="navButton" ID="Button3" Text="Type Wise Report" runat="server" PostBackUrl="TypeWiseReport.aspx" ></asp:Button></a>
        </div>

        <div style="height: 80px;" >
        </div>

    <div class="CenterDivStyle">
        <fieldset class="FiedSetStyle">
            <legend class="LegendStyle">Payment</legend>
            <table class="TableStyle">
                <tr>
                    <td>
                        <asp:label ID="billNo" runat="server" class="TdLabelStyle">Bill No</asp:label>
                        <asp:textbox id="billNoTxt" runat="server" class="TdTextBoxStyle" ></asp:textbox>
                        <asp:label ID="Label1" runat="server" >Or</asp:label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:label ID="mobile" runat="server" class="TdLabelStyle">Mobile</asp:label>
                        <asp:textbox ID="mobileTxt" runat="server" class="TdTextBoxStyle"></asp:textbox>
                        <asp:Button ID="searchButton" runat="server" Text="Search" class="TableButtonStyle" OnClick="searchButton_Click"></asp:Button>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:label ID="message" runat="server" class="OperationMessage"></asp:label>
                    </td>
                </tr>
            </table>
            <table class="TableStyle">
                <tr>
                    <td>
                        <asp:label ID="amount" runat="server" class="TdLabelStyle">Amount</asp:label>
                        <asp:textbox id="amountTxt" runat="server" class="TdTextBoxStyle"></asp:textbox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:label ID="dueDate" runat="server" class="TdLabelStyle">Due date</asp:label>
                        <asp:textbox ID="dueDateTxt" runat="server" class="TdTextBoxStyle"></asp:textbox>
                        <asp:Button ID="payButton" runat="server" Text="Pay" class="TableButtonStyle" OnClick="payButton_Click"></asp:Button>
                    </td>
                </tr>
                 <tr>
                    <td>
                        <asp:label ID="payMessage" runat="server" class="OperationMessage"></asp:label>
                    </td>
                </tr>
                <div style="height: 25px;" >
                </div>
            </table>
            <div style="height: 100px;" >
            </div>
        </fieldset>
    </div>
    </form>
</body>
</html>

