﻿using DiagnosticCenterBillManagementApp.BLL;
using DiagnosticCenterBillManagementApp.DAL;
using DiagnosticCenterBillManagementApp.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DiagnosticCenterBillManagementApp
{
    public partial class TestInfoForm : System.Web.UI.Page
    {
        TestManager testManager = new TestManager();
        TestTypeManager testTypeManager = new TestTypeManager();

        protected void Page_Load(object sender, EventArgs e)
        {
            LoadTestInGrid();

            if (!IsPostBack)
            {
                testTypeDDL.Items.Clear();
                LoadTestTypeInDDL();
            }

            if (testInfoGridView.Rows.Count <= 0)
                gridDiv.Visible = false;
            message.Text = "";
        }

        protected void LoadTestInGrid()
        {
            if (testManager.GetTestList().Count > 0)
            {
                testInfoGridView.DataSource = testManager.GetTestList();
                testInfoGridView.DataBind();
                gridDiv.Visible = true;
            }
            else
            {
                emptyMessage.Text = "No Record Found!!";
            }
        }

        public void LoadTestTypeInDDL()
        {
            testTypeDDL.DataSource = testTypeManager.GetTestList();
            testTypeDDL.DataValueField = "Type";
            testTypeDDL.DataTextField = "Type";
            testTypeDDL.Items.Insert(0, new ListItem("--- Please Select Test Type ---", "0"));
            testTypeDDL.DataBind();
        }

        private void ClearTextBoxs()
        {
            testNameTxt.Text = null;
            feeTxt.Text = null;
            testTypeDDL.SelectedIndex = 0;
            emptyMessage.Text = null;
        }

        protected void save_Click(object sender, EventArgs e)
        {
            try
            {
                if (testNameTxt.Text == "" || feeTxt.Text == "" || testTypeDDL.SelectedItem.Value=="0")
                message.Text = "Please Give all inputs First!!";
                else
                {
                    Test test = new Test(testNameTxt.Text, Convert.ToDecimal(feeTxt.Text), testTypeDDL.SelectedItem.Value);
                    ClearTextBoxs();

                    int inserted = testManager.Insert(test);
                    if (inserted == 1)
                    {
                        message.Text = "Inserted Succesfully!!";
                        LoadTestInGrid();
                    }
                    else
                        message.Text = "Insertion Failed!!";
                }
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
        }
    }
}