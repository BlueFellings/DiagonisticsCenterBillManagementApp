﻿using DiagnosticCenterBillManagementApp.BLL;
using DiagnosticCenterBillManagementApp.DAL;
using DiagnosticCenterBillManagementApp.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DiagnosticCenterBillManagementApp.UI
{
    public partial class SetTestTypeForm : System.Web.UI.Page
    {
        TestTypeManager testTypeManager = new TestTypeManager();
        protected void Page_Load(object sender, EventArgs e)
        {
            LoadTestNamesInGrid();
        }

        protected void LoadTestNamesInGrid()
        {
            if (testTypeManager.GetTestList().Count > 0)
            {
                testGridView.DataSource = testTypeManager.GetTestList();
                testGridView.DataBind();
            }
            else
            {
                emptyMessage.Text = "No Record Found!!";
            }
        }

        protected void SaveButton_Click(object sender, EventArgs e)
        {
            if (testNameTxt.Text == "")
                message.Text = "Please Give a Test Type First!!";
            else 
            {
                TestType testType = new TestType(testNameTxt.Text);
                ClearTextBoxs();
                if (testNameTxt.Text != null)
                {
                    try
                    {
                        int inserted = testTypeManager.Insert(testType);
                        if (inserted == 1)
                        {
                            message.Text = "Inserted Succesfully!!";
                            LoadTestNamesInGrid();
                        }
                        else
                        {
                            message.Text = "Insertion Failed!!";
                        }
                    }
                    catch (Exception ex)
                    {
                        message.Text = ex.Message;
                    }
                }
                else
                    message.Text = "Give a Type Name First!!";
            }
            
        }

        private void ClearTextBoxs()
        {
            testNameTxt.Text = null;
            emptyMessage.Text = null;
        }
    }
}