﻿using DiagnosticCenterBillManagementApp.BLL;
using DiagnosticCenterBillManagementApp.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DiagnosticCenterBillManagementApp.UI
{
    public partial class Payment : System.Web.UI.Page
    {
        PaymentManager paymentManager = new PaymentManager();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void searchButton_Click(object sender, EventArgs e)
        {
            payMessage.Text = "";
            amountTxt.Text = "";
            dueDateTxt.Text = "";
            if (billNoTxt.Text != "" && mobileTxt.Text != "")
            {
                message.Text = "Please Select only One Criteria !!";
            }
            else if (billNoTxt.Text != "")
            {
                TestRequest testRequest = paymentManager.GetPayingAmount("mobile", Convert.ToInt32(billNoTxt.Text));
                amountTxt.Text = testRequest.Fee.ToString();
                if (amountTxt.Text == "0") 
                {
                    dueDateTxt.Text = "";
                }
                else
                    dueDateTxt.Text = testRequest.EntryDate.ToString();
            }
            else if (mobileTxt.Text != "") 
            {
                TestRequest testRequest = paymentManager.GetPayingAmount(mobileTxt.Text, 0);
                dueDateTxt.Text = testRequest.EntryDate.ToString();
                amountTxt.Text = testRequest.Fee.ToString();
            }
            else
            {
                message.Text = "Please Give a Seraching Criteria!!";
            }
        }

        protected void payButton_Click(object sender, EventArgs e)
        {
            int bill = 0;
            string mobile = "0";

            try
            {
                if(mobileTxt.Text != "")
                    mobile = mobileTxt.Text;
                if(billNoTxt.Text!= "")
                    bill = Convert.ToInt32(billNoTxt.Text);
                if (mobile == "0" && bill == 0)
                {
                    payMessage.Text = "Please Give Searching Criteria First !!";
                }
                else 
                {
                    if (Convert.ToDecimal(amountTxt.Text) > 0)
                    {
                        int updated = paymentManager.UpdateRequestEntry(mobile, bill);
                        if (updated > 0)
                        {
                            payMessage.Text = "Paid Succesfully!!";
                        }
                        else if (updated <= 0)
                        {
                            payMessage.Text = "Bill Payment Failed!!";
                        }
                    }
                    else
                        payMessage.Text = "No Due Bill !!";
                }
                ClearTextBoxs();
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
        }

       

        private void ClearTextBoxs()
        {
            mobileTxt.Text = "";
            billNoTxt.Text = "";
            message.Text = "";
            dueDateTxt.Text = "";
            amountTxt.Text = "";
        }
    }
}