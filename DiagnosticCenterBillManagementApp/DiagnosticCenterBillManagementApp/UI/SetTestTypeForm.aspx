﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SetTestTypeForm.aspx.cs" Inherits="DiagnosticCenterBillManagementApp.UI.SetTestTypeForm" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Test Type Setup</title>
    <link href="../Style/CommonStyle.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        
        <div id="navigation">
	    <a href ="SetTestTypeForm.aspx" runat="server" class="LinkButton"><asp:Button class="navButton" ID="testName" runat="server" Text="Test Type Setup" PostBackUrl="SetTestTypeForm.aspx"></asp:Button></a>|
	    <a href ="TestInfoForm.aspx" runat="server" class="LinkButton"><asp:Button class="navButton" ID="testInfo" runat="server" Text="Test Setup" PostBackUrl="TestInfoForm.aspx"></asp:Button></a>|
	    <a href ="PatientsTestEntryForm.aspx" runat="server" class="LinkButton"><asp:Button class="navButton" ID="patientTestEntry" Text="Test Request Entry" runat="server" PostBackUrl="PatientsTestEntryForm.aspx" ></asp:Button></a>|
        <a href ="Payment.aspx" runat="server" class="LinkButton"><asp:Button class="navButton" ID="Button1" Text="Payment" runat="server" PostBackUrl="Payment.aspx" ></asp:Button></a>|
        <a href ="UnpaidBill.aspx" runat="server" class="LinkButton"><asp:Button class="navButton" ID="Button4" Text="Unpaid Bill" runat="server" PostBackUrl="UnpaidBill.aspx" ></asp:Button></a>|
        <a href ="TestWiseReport.aspx" runat="server" class="LinkButton"><asp:Button class="navButton" ID="Button2" Text="Test Wise Report" runat="server" PostBackUrl="TestWiseReport.aspx" ></asp:Button></a>|
        <a href="TypeWiseReport.aspx" runat="server" class="LinkButton"><asp:Button class="navButton" ID="Button3" Text="Type Wise Report" runat="server" PostBackUrl="TypeWiseReport.aspx" ></asp:Button></a>
        </div>

        <div style="height: 80px;" >
        </div>

    <div class="CenterDivStyle">
        <fieldset class="FiedSetStyle">
            <legend class="LegendStyle">Test Type Setup</legend>
            <table class="TableStyle">
                <tr>
                    <td>
                        <asp:label ID="name" runat="server" CssClass="TdLabelStyle">Type Name</asp:label>
                        <asp:textbox id="testNameTxt" runat="server" class="TdTextBoxStyle"></asp:textbox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="save" runat="server" Text="Save" CssClass="TableButtonStyle" OnClick="SaveButton_Click"></asp:Button>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:label ID="message" runat="server" class="OperationMessage"></asp:label>
                    </td>
                </tr>
            </table>
            <div id="gridDiv" class="TableStyle" >
                <asp:GridView ID="testGridView" runat="server" AutoGenerateColumns="false" BackColor="White" BorderColor="#999999" 
                    BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Vertical" Width="100%">
                    <AlternatingRowStyle BackColor="#a7c6a5" />
                    <Columns>
                        <asp:TemplateField HeaderText="SL" HeaderStyle-Width="20%" ItemStyle-Width="20%">
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# Bind("TestSl") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Type Name" HeaderStyle-Width="80%" ItemStyle-Width="80%">
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# Bind("Type") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />  
                    <HeaderStyle BackColor="#186d04" Font-Bold="True" ForeColor="White" />  
                    <PagerStyle BackColor="#808080" ForeColor="Black" HorizontalAlign="Center" />  
                    <RowStyle BackColor="#98dc93" ForeColor="Black" />  
                    <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="Black" />  
                    <SortedAscendingCellStyle BackColor="#F1F1F1" />  
                    <SortedAscendingHeaderStyle BackColor="#0000A9" />  
                    <SortedDescendingCellStyle BackColor="#CAC9C9" />  
                    <SortedDescendingHeaderStyle BackColor="#000065" />
                </asp:GridView>
                <asp:Label ID="emptyMessage" runat="server" Text=""></asp:Label>
            </div>
            <div style="height: 100px;" >
            </div>
        </fieldset>
    </div>
    </form>
</body>
</html>

